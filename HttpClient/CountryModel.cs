﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HttpClientApi
{
    public class CountryModel : IEquatable<CountryModel>
    {
        public string Name { get; set; }

        public bool Equals(CountryModel other) => other != null && Name == other.Name;

        public override int GetHashCode() => HashCode.Combine(Name);
    }
}
