﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpClientApi
{
    public class HttpClientHandler : IHttpClientHandler
    {
        private readonly HttpClient httpClient;

        public async Task<string> GetDataAsync() => 
            await httpClient.GetStringAsync("");
    }
}
