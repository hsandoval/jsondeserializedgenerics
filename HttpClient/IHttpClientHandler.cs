﻿using System.Threading.Tasks;

namespace HttpClientApi
{
    public interface IHttpClientHandler
    {
        Task<string> GetDataAsync();
    }
}
