﻿using System;

namespace HttpClientApi
{
    public class PersonModel : IEquatable<PersonModel>
    {
        public int NumberIdentification { get; set; }
        public string FirtsName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }

        public bool Equals(PersonModel toCompareWith) =>
                (
                    NumberIdentification == toCompareWith.NumberIdentification &&
                    FirtsName == toCompareWith.FirtsName &&
                    SecondName == toCompareWith.SecondName &&
                    LastName == toCompareWith.LastName
                );

        public override int GetHashCode() => HashCode.Combine(NumberIdentification);
    }
}