﻿using Newtonsoft.Json;
using System.Threading.Tasks;

namespace HttpClientApi
{
    public class JsonStringDeserializeObject<T>
    {
        public Task<T> GetObjectDeserialized(string jsonData, T classExpected)
        {
            T data = JsonConvert.DeserializeObject<T>(jsonData);
            return Task.FromResult(data);
        }
    }
}
