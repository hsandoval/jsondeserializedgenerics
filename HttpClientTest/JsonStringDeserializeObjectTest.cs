using HttpClientApi;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tests
{
    public class JsonStringDeserializeObjectTest
    {
        [TestCaseSource("PersonsSomeTestCases")]
        public async Task ConvertJsonToPersonModel(string jsonData, PersonModel expectedValue)
        {
            JsonStringDeserializeObject<PersonModel> objectDeserialized = new JsonStringDeserializeObject<PersonModel>();

            PersonModel result = await objectDeserialized.GetObjectDeserialized(jsonData, expectedValue);
            Assert.AreEqual(result, expectedValue);
        }

        [TestCaseSource("CountriesSomeTestCases")]
        public async Task ConvertJsonToCountryModel(string jsonData, CountryModel expectedValue)
        {
            JsonStringDeserializeObject<CountryModel> objectDeserialized = new JsonStringDeserializeObject<CountryModel>();

            CountryModel result = await objectDeserialized.GetObjectDeserialized(jsonData, expectedValue);
            Assert.AreEqual(result, expectedValue);
        }

        [TestCase("true", true, Category = "UnitTest", TestName = "ConvertTrueToBoolean")]
        [TestCase("false", false, Category = "UnitTest", TestName = "ConvertFalseToBoolean")]
        public async Task GetBooleanDeserializedTest(string jsonData, bool expectedValue)
        {
            JsonStringDeserializeObject<bool> objectDeserialized = new JsonStringDeserializeObject<bool>();

            bool result = await objectDeserialized.GetObjectDeserialized(jsonData, expectedValue);
            Assert.AreEqual(result, expectedValue);
        }


        [TestCase("195", 195, Category = "UnitTest", TestName = "ConvertShortNumberStringToShortNumber")]
        [TestCase("20301270", 20301270, Category = "UnitTest", TestName = "ConvertLongNumberStringToLongNumber")]
        public async Task GetNumberDeserializedTest(string jsonData, int expectedValue)
        {
            JsonStringDeserializeObject<int> objectDeserialized = new JsonStringDeserializeObject<int>();

            int result = await objectDeserialized.GetObjectDeserialized(jsonData, expectedValue);
            Assert.AreEqual(result, expectedValue);
        }

        private static IEnumerable<TestCaseData> PersonsSomeTestCases {
            get {
                yield return new TestCaseData(
                        @"{'NumberIdentification':'20301270', 'FirtsName':'Henk', 'SecondName':'Alexander', 'LastName':'Sandoval', }",
                        new PersonModel { NumberIdentification = 20301270, FirtsName = "Henk", SecondName = "Alexander", LastName = "Sandoval" }
                    ).SetName("GetFirtsPerson");
            }
        }

        private static IEnumerable<TestCaseData> CountriesSomeTestCases {
            get {
                yield return new TestCaseData(
                        @"{'Name':'Germany' }",
                        new CountryModel { Name = "Germany" }
                    ).SetName("GetGermany");
                yield return new TestCaseData(
                        @"{'Name':'Brazil' }",
                        new CountryModel { Name = "Brazil" }
                    ).SetName("GetBrazil");
                yield return new TestCaseData(
                        @"{'Name':'Spain' }",
                        new CountryModel { Name = "Spain" }
                    ).SetName("GetSpain");
            }
        }
    }
}